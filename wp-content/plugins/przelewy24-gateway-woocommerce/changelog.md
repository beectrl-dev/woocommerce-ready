# ChangeLog

## [3.2.9] - 2016-10-07

- Rename config.js to admin.js

## [3.2.8] - 2016-10-07

- Drop custom data table after uninstall plugin

## [3.2.7] - 2016-10-05

- Fixed notices. 
- Uninstall hook with delete_option('woocommerce_przelewy24_settings'),

## [3.2.6] - 2016-09-20

- Fixed Zencard_Util number format amount.