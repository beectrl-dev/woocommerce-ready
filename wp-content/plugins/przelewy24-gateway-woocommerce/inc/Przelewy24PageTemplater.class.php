<?php

class Przelewy24PageTemplater
{
    private function __construct()
    {
    }

    public static function includeTemplate($name, $data)
    {
        $name = str_replace('/', '', $name);

        if ($theme_file = locate_template(array($name . '.tpl.php'))) {
            $template_path = $theme_file;
        } else {
            $template_path = PRZELEWY24_PATH . '/template/' . $name . '.tpl.php';
        }
        ob_start();
        include $template_path;
        $result = ob_get_clean();
        return $result;
    }
}