<?php


class WC_Gateway_Przelewy24 extends WC_Payment_Gateway
{
    public function __construct()
    {
        global $woocommerce;

        $this->id = 'przelewy24';
        $this->icon = PRZELEWY24_URI . 'logo.png';
        $this->medthod_title = 'Przelewy24';
        $this->has_fields = false;

        $this->init_settings();

        $this->title = (isset($this->settings['title']) ? $this->settings['title'] : '');
        $this->description = (isset($this->settings['description'])) ? $this->settings['description'] : '';
        $this->instructions = $this->get_option('instructions', $this->description);
        $this->merchant_id = (isset($this->settings['merchant_id'])) ? $this->settings['merchant_id'] : 0;
        $this->shop_id = (isset($this->settings['shop_id'])) ? $this->settings['shop_id'] : 0;
        $this->salt = (isset($this->settings['CRC_key'])) ? $this->settings['CRC_key'] : '';
        $this->p24_testmod = (isset($this->settings['p24_testmod']) ? $this->settings['p24_testmod'] : 0);
        $this->p24_debug = (isset($this->settings['p24_debug']) ? $this->settings['p24_debug'] : 0);
        $this->p24_api = (isset($this->settings['p24_api']) ? $this->settings['p24_api'] : '');
        $this->p24_timelimit = (isset($this->settings['p24_timelimit']) ? $this->settings['p24_timelimit'] : 15);
        $this->p24_payslow = (isset($this->settings['p24_payslow']) ? $this->settings['p24_payslow'] : 'no');
        $this->p24_oneclick = (isset($this->settings['p24_oneclick']) ? $this->settings['p24_oneclick'] : 'no');
        $this->p24_installment = (isset($this->settings['p24_installment']) ? $this->settings['p24_installment'] : 0);
        $this->p24_orderstate_before = (isset($this->settings['p24_orderstate_before']) ? $this->settings['p24_orderstate_before'] : '');
        $this->p24_orderstate_after = (isset($this->settings['p24_orderstate_after']) ? $this->settings['p24_orderstate_after'] : '');
        $this->p24_extracharge = (isset($this->settings['p24_extracharge']) ? $this->settings['p24_extracharge'] : 'no');
        $this->p24_extracharge_amount = (isset($this->settings['p24_extracharge_amount']) ? $this->settings['p24_extracharge_amount'] : 0);
        $this->p24_extracharge_percent = (isset($this->settings['p24_extracharge_percent']) ? $this->settings['p24_extracharge_percent'] : 0);
        $this->p24_basicauth = (isset($this->settings['p24_basicauth']) ? $this->settings['p24_basicauth'] : '');

        $this->init_form_fields();

        if (version_compare(WOOCOMMERCE_VERSION, '2.0.0', '>=')) {
            add_action('woocommerce_update_options_payment_gateways_' . $this->id, array(&$this, 'process_admin_options'));
        } else {
            add_action('woocommerce_update_options_payment_gateways', array(&$this, 'process_admin_options'));
        }
        add_action('woocommerce_receipt_przelewy24', array(&$this, 'receipt_page'));
        add_action('woocommerce_thankyou_przelewy24', array($this, 'thankyou_page'));
        add_action('woocommerce_email_before_order_table', array($this, 'email_instructions'), 10, 3);
        add_action('admin_enqueue_scripts', array($this, 'load_custom_scripts'));

        //Listener API
        add_action('woocommerce_api_wc_gateway_przelewy24', array($this, 'przelewy24_response'));
        //  if (isset($_SESSION['P24'])) $this->sanitized_fields = $_SESSION['P24'];
    }

    function load_custom_scripts($hook)
    {
        if (empty($_REQUEST['section']) || 'woocommerce_page_wc-settings' != $hook || strpos($_REQUEST['section'], 'przelewy24') === false) {
            return;
        }
        wp_enqueue_script('jquery_script', '//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js');
        wp_enqueue_script('jqueryui_script', '//ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js');
        wp_enqueue_style('jqueryui_css', '//ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/smoothness/jquery.ui.all.css');
        wp_enqueue_style('p24_plugin_css', PRZELEWY24_URI . 'assets/css/paymethods.css');
        wp_enqueue_style('p24_css', 'https://secure.przelewy24.pl/skrypty/ecommerce_plugin.css.php');
        wp_enqueue_script('p24_payment_script', PRZELEWY24_URI . 'assets/js/admin.js');
    }

    function init_form_fields()
    {
        $this->form_fields = array(
            'enabled' => array(
                'title' => __('Włącz/Wyłącz'),
                'type' => 'checkbox',
                'label' => __('Aktywuj moduł płatności Przelewy24.'),
                'default' => 'no'),
            'title' => array(
                'title' => __('Title:'),
                'type' => 'text',
                'description' => __('Tekst który zobaczą klienci podczas dokonywania zakupu'),
                'default' => __('Przelewy24')),
            'merchant_id' => array(
                'title' => __('ID Sprzedawcy'),
                'type' => 'text',
                'description' => __('Identyfikator sprzedawcy nadany w systemie Przelewy24.'),
                'default' => 0,
                'required' => true),
            'shop_id' => array(
                'title' => __('ID Sklepu'),
                'type' => 'text',
                'description' => __('Identyfikator sklepu nadany w systemie Przelewy24.'),
                'default' => 0,
                'required' => true),
            'CRC_key' => array(
                'title' => __('Klucz CRC'),
                'type' => 'text',
                'description' => __('Klucz do CRC nadany w systemie Przelewy24.'),
                'placeholder' => __('(16 znaków)'),
                'required' => true),
            'p24_testmod' => array(
                'title' => __('Tryb modułu'),
                'type' => 'select',
                'options' => $this->get_options(),
                'description' => __('Tryb przeprowadzania transakcji')),
            'description' => array(
                'title' => __('Description', 'woocommerce'),
                'type' => 'textarea',
                'description' => __('Tekst który zobaczą klienci przy wyborze metody płatności'),
                'default' => __('Płać z Przelewy24')),
            'instructions' => array(
                'title' => __('Instructions', 'woocommerce'),
                'type' => 'textarea',
                'description' => __('Tekst który zobaczą klienci po dokonaniu zakupu oraz w mailu z potwierdzeniem'),
                'default' => __('Dziękujemy za skorzystanie z płatności Przelewy24.')),
            'p24_debug' => array(
                'title' => __('Tryb debugowania'),
                'type' => 'checkbox',
                'description' => __('Tryb do testowania poprawności działania modułu')),
            'p24_basicauth' => array(
                'title' => __('Mechanizm Basic-Auth'),
                'type' => 'text',
                'description' => __('Jeśli Twój sklep używa mechanizmu Basic-Auth, podaj tutaj login i hasło'),
                'placeholder' => __('login:hasło')),
            'p24_orderstate_before' => array(
                'title' => __('Status przed dokonaniem płatności'),
                'type' => 'select',
                'default' => 'wc-pending',
                'options' => wc_get_order_statuses()),
            'p24_orderstate_after' => array(
                'title' => __('Status po dokonaniu płatności'),
                'type' => 'select',
                'default' => 'wc-processing',
                'options' => wc_get_order_statuses()),
            'p24_api' => array(
                'title' => __('Klucz API'),
                'type' => 'text',
                'description' => __('Klucz API należy pobrać z panelu Przelewy24 z zakładki Moje dane'),
                'placeholder' => __('(32 znaki)')),
            'p24_oneclick' => array(
                'title' => __('Oneclick'),
                'type' => 'checkbox',
                'label' => __('Aktywuj płatności oneclick'),
                'default' => 'no'),
            'p24_payinshop' => array(
                'title' => __('Płatność w sklepie'),
                'type' => 'checkbox',
                'label' => __('Płatność kartą wewnątrz sklepu'),
                'default' => 'no'),
            'p24_accept' => array(
                'title' => __('Regulamin'),
                'label' => __('Akceptacja regulaminu przelewy24.pl'),
                'type' => 'checkbox',
                'description' => __('Pokazuje przycisk akceptacji regulaminu przelewy24.pl wewnątrz sklepu'),
                'default' => 'no'),
            'p24_installment' => array(
                'title' => __('Ustawienia płatności ratalnej'),
                'type' => 'select',
                'default' => '0',
                'options' => $this->get_options_installment()),
            'p24_payslow' => array(
                'type' => 'checkbox',
                'title' => __('Liczba kanałów płatności'),
                'label' => __('Pokaż tylko płatności natychmiastowe'),
                'description' => __('To ustawienie ogranicza liczbę dostępnych metod płatności'),
                'default' => 'no'),
            'p24_timelimit' => array(
                'type' => 'number',
                'title' => __('Limit czasu na transakcję'),
                'description' => __('Wartość 0 oznacza brak limitu czasu'),
                'default' => 15),
            'p24_paymethods' => array(
                'type' => 'checkbox',
                'title' => __('Pokaż metody płatności'),
                'label' => __('Pokaż dostępne metody płatności w sklepie'),
                'description' => __('Klient może wybrać metodę płatności na stronie potwierdzenia zamówienia'),
                'default' => 'no'),
            'p24_graphics' => array(
                'type' => 'checkbox',
                'label' => __('Użyj graficznej listy metod płatności'),
                'default' => 'yes'),

            'p24_paymethods_first' => array(
                'type' => 'text',
                'title' => __('Widoczne metody płatności'),
                'default' => ''),
            'p24_paymethods_second' => array(
                'type' => 'text',
                'title' => __(''),
                'default' => ''),
            'p24_paymethods_all' => array(
                'type' => 'select',
                'options' => $this->get_all_payment_methods(),
                'default' => 0),

            'p24_extracharge' => array(
                'title' => __('Opłata dodatkowa'),
                'type' => 'checkbox',
                'label' => __('Naliczaj opłatę dodatkową'),
                'default' => 'no'),
            'p24_extracharge_product' => array(
                'title' => __('Wybierz produkt wirtualny'),
                'type' => 'select',
                'options' => $this->get_all_vurtual_products(),
                'label' => __('Ten produkt zostanie dopisany do zamówienia'),
                'default' => 0),
            'p24_extracharge_amount' => array(
                'title' => __('Zwiększ płatność o kwotę'),
                'type' => 'number',
                'default' => 0),
            'p24_extracharge_percent' => array(
                'title' => __('Zwiększ płatność o procent'),
                'type' => 'number',
                'default' => 0,
                'description' => __('Do płatności zostanie doliczona jedynie większa z tych kwot')),
            'p24_wait_for_result' => array(
                'title' => __('Czekaj na wynik transakcji'),
                'type' => 'checkbox',
                'default' => 'no',
                'label' => ' '),
            'ZENCARD_PRZELEWY24_MODE' => array(
                'title' => __('ZenCard włączony'),
                'type' => 'checkbox',
                'default' => 'no',
                'label' => __('Włączenie ZenCard'))
        );
    }

    public function get_all_payment_methods($pay_slow = false)
    {
        $P24 = new Przelewy24Class($this->merchant_id, $this->shop_id, $this->salt, ($this->p24_testmod == 'sandbox'), $this->p24_api);
        $all = $P24->availablePaymentMethodsSimple($pay_slow);
        return $all;
    }

    public function get_all_vurtual_products()
    {
        $all = array();
        $products = get_posts(array('post_type' => 'product', 'meta_key' => '_virtual', 'meta_value' => 'yes'));
        foreach ($products as $product) {
            $all[$product->ID] = $product->post_title;
        }
        return $all;
    }

    public function validate_payment_methods_status($key)
    {
        $keyVal = "0";
        if (isset($_POST[$this->plugin_id . $this->id . '_' . $key])) {
            $keyVal = $_POST[$this->plugin_id . $this->id . '_' . $key];
            if ($keyVal) {
                try {
                    $P24 = new Przelewy24Class($this->sanitized_fields['merchant_id'], $this->sanitized_fields['shop_id'], $this->sanitized_fields['CRC_key'], ($this->sanitized_fields['p24_testmod'] == 'sandbox'), $this->sanitized_fields['p24_api']);
                    $address = $P24->getHost() . 'external/' . $this->sanitized_fields['merchant_id'] . '.wsdl';
                    $client = new SoapClient($address, array('trace' => true, 'exceptions' => true));
                    if (!$client->PaymentMethods($this->sanitized_fields['merchant_id'], $this->sanitized_fields['p24_api'], 'PL')) {
                        throw new Exception();
                    }
                } catch (Exception $ex) {
                    error_log(__METHOD__ . ' ' . $ex->getMessage());
                    $_POST[$this->plugin_id . $this->id . '_' . $key] = "0";
                    $this->add_error("Usługa PaymentMethods nie jest włączona dla tego sprzedawcy.");
                    return "no";
                }
            }
        }
        return $keyVal ? "yes" : "no";
    }

    public function validate_id($key, $error)
    {
        $ret = $this->get_option($key);
        $valid = false;
        if (isset($_POST[$this->plugin_id . $this->id . '_' . $key])) {
            $ret = $_POST[$this->plugin_id . $this->id . '_' . $key];
            if (is_numeric($ret) && $ret >= 1000) $valid = true;
        }
        if (!$valid) $this->errors[$key] = $error;
        return $ret;
    }

    public function validate_crc($key)
    {
        $ret = $this->get_option($key);
        $valid = false;
        if (isset($_POST[$this->plugin_id . $this->id . '_' . $key])) {
            $ret = $_POST[$this->plugin_id . $this->id . '_' . $key];
            if (strlen($ret) == 16 && ctype_xdigit($ret)) $valid = true;
        }
        if (!$valid) $this->errors[$key] = 'Klucz do CRC powinien mieć 16 znaków.';
        return $ret;
    }

    public function validate_zencard($key)
    {
        $ret = "0";
        $valid = true;
        if (isset($_POST[$this->plugin_id . $this->id . '_' . $key])) {
            $zenCardApi = new ZenCardApi(
                $_POST[$this->plugin_id . $this->id . '_merchant_id'],
                $_POST[$this->plugin_id . $this->id . '_p24_api']
            );
            if (!$apiEnabled = $zenCardApi->isEnabled()) {
                $valid = false;
                $_POST[$this->plugin_id . $this->id . '_' . $key] = $ret;
            } else if ($apiEnabled) {
                $ret = $_POST[$this->plugin_id . $this->id . '_' . $key];
            }
        }

        if (!$valid) $this->errors[$key] = 'ZenCard nie jest aktywny dla tego sprzedawcy.';
        return $ret ? 'yes' : "no";
    }

    public function get_option($key, $empty_value = null)
    {
        if (isset($this->sanitized_fields[$key])) {
            return $this->sanitized_fields[$key];
        }
        return parent::get_option($key, $empty_value);
    }

    public function display_errors()
    {
        foreach ($this->errors as $v) {
            echo '<div class="error">Błąd: ' . $v . '</div>';
        }
        echo '<script type="text/javascript">jQuery(document).ready(function () {jQuery(".updated").remove();});</script>';
    }

    public function add_error($error)
    {
        if (!in_array($error, $this->errors)) {
            parent::add_error($error);
        }
    }

    public function process_admin_options()
    {
        parent::process_admin_options();
        if (version_compare(WOOCOMMERCE_VERSION, '2.6.4', '>=')) {
            $this->validate_settings_fields();
            if (!empty($this->errors)) {
                $this->display_errors();
            }
        }
    }

    public function validate_settings_fields($form_fields = false)
    {
        if (!$form_fields) $form_fields = $this->get_form_fields();
        $this->sanitized_fields['p24_testmod'] = $_POST[$this->plugin_id . $this->id . '_p24_testmod'] == 'secure' ? 'secure' : 'sandbox';
        $this->sanitized_fields['merchant_id'] = $this->validate_id('merchant_id', 'Błędny ID Sprzedawcy.');
        $this->sanitized_fields['shop_id'] = $this->validate_id('shop_id', 'Błędny ID Sklepu.');
        $this->sanitized_fields['CRC_key'] = $this->validate_crc('CRC_key');
        $this->sanitized_fields['ZENCARD_PRZELEWY24_MODE'] = $this->validate_zencard('ZENCARD_PRZELEWY24_MODE');
        $this->sanitized_fields['p24_api'] = $_POST[$this->plugin_id . $this->id . '_p24_api'];
        $this->sanitized_fields['p24_oneclick'] = $this->validate_payment_methods_status('p24_oneclick');
        $this->sanitized_fields['p24_paymethods'] = $this->validate_payment_methods_status('p24_paymethods');

        if (version_compare(WOOCOMMERCE_VERSION, '2.6', '>=')) {
            parent::validate_fields($form_fields);
        } else {
            parent::validate_settings_fields($form_fields);
        }

        $P24 = new Przelewy24Class($this->sanitized_fields['merchant_id'], $this->sanitized_fields['shop_id'], $this->sanitized_fields['CRC_key'], ($this->sanitized_fields['p24_testmod'] == 'sandbox'), $this->sanitized_fields['p24_api']);

        $ret = $P24->testConnection();
        if ($ret['error'] != 0)
            $this->errors['p24_testmod'] = 'Błędny ID Sklepu, Sprzedawcy lub Klucz do CRC dla tego trybu pracy wtyczki.';


        if (!empty($this->sanitized_fields['p24_api'])) {
            $ret = $P24->apiTestAccess();
            if (!$ret)
                $this->errors['p24_testmod'] = 'Błędny klucz API dla tego ID Sklepu, Sprzedawcy lub trybu pracy wtyczki.';
        }

        $_SESSION['P24'] = $this->sanitized_fields;
    }

    public function admin_options()
    {
        echo '<h3>' . __('Bramka płatności Przelewy24') . '</h3>';
        echo '<table class="form-table">';
        // Generate the HTML For the settings form.
        $this->generate_settings_html();
        echo '</table>';

        $P24 = new Przelewy24Class($this->settings['merchant_id'], $this->settings['shop_id'], $this->settings['CRC_key'], ($this->settings['p24_testmod'] == 'sandbox'), $this->p24_api);

        if (!$P24->apiTestAccess()) {
            echo '<input type="hidden" id="p24_no_api_key_provided">';
        }
    }

    function payment_fields()
    {
        if ($this->description) {
            echo wpautop(wptexturize($this->description));
        }

        $extracharge_amount = $this->getExtrachargeAmount($this->get_order_total()) / 100;

        if ($extracharge_amount > 0) {
            echo wpautop(wptexturize(__('Do płatności zostanie doliczona kwota') . ' ' . woocommerce_price($extracharge_amount)));
        }

        // raty na liście bramek płatności
        if (get_woocommerce_currency() == 'PLN' && $this->get_order_total() >= Przelewy24Class::getMinRatyAmount() && $this->p24_installment > 0) {
            $all = $this->get_all_payment_methods($conf['p24_payslow'] == 'yes');
            $raty = Przelewy24Class::getChannelsRaty();
            $ratyArr = array();
            foreach ($raty as $val) {
                if (isset($all[$val])) $ratyArr[$val] = $all[$val];
            }
            if (sizeof($ratyArr) > 0) {
                $ratyJson = json_encode($ratyArr);
                echo <<<HTML
                    <script>
                            jQuery(document).ready(function(){
                                if (jQuery('.payment_methods[data-p24installment-procesed]').length == 0) {
                                    var dummy = jQuery('.payment_methods')
                                        .attr('data-p24installment-procesed', true)
                                        .find('li.payment_method_przelewy24')
                                        .clone()
                                    ;
                                    dummy.find('div,script,img').remove();
                                    dummy.find('input').attr('data-val', dummy.find('input').val());
                                    var ratyJson = JSON.parse('{$ratyJson}');
                                    for (i in ratyJson) {
                                        var item = dummy.clone();
                                        item.find('label').html(ratyJson[i]).attr('for','raty-' + i);;
                                        item.find('input').val('raty-' + i).attr('id','raty-' + i).attr('data-method', i);
                                        jQuery('.payment_methods').prepend(item);
                                    }
                                    jQuery('input[name=woocommerce_checkout_place_order]').click(function(){
                                        var ob = jQuery('.payment_methods input:checked[data-val]');
                                        var method = ob.attr('data-method');
                                        jQuery('form[name=checkout]').prepend('<input type="hidden" name="p24_method" id="p24_method">');
                                        jQuery('#p24_method').val(method);
                                        ob.val(ob.attr('data-val'));
                                    });
                                }
                            });
                    </script>
HTML;
            }
        }
    }

    /**
     * Receipt Page
     **/
    function receipt_page($order)
    {
        global $woocommerce;

        if (isset($_POST['act']) && $_POST['act'] == 'cardrm' && isset($_POST['cardrm']) && (int)$_POST['cardrm'] > 0) {
            self::del_card(get_current_user_id(), (int)$_POST['cardrm']);
        }

        $conf = $this->settings;

        $orderObj = new WC_Order($order);
        $orderObj->update_status($this->p24_orderstate_before);

        if (!empty($_POST['p24_cc']) && !empty($_POST['p24_session_id'])) {
            $result = $this->chargeCard($order, $_POST['p24_cc']);

            if ($result) {
                $orderObj->add_order_note(__('IPN payment completed', 'woocommerce'));
                $orderObj->payment_complete();
                $woocommerce->cart->empty_cart();
                $orderObj->update_status($this->p24_orderstate_after);
                //wp_redirect($orderObj->get_view_order_url());
                //$this->orderDone($orderObj);
                wp_redirect($this->get_return_url($orderObj));
            } else {
                //Sorry your transaction did not go through successfully, please try again.
                $this->addNotice(
                    $woocommerce,
                    __('Payment error: ', 'woothemes') . 'Przepraszamy, ale twoja transakcja nie została przeprowadzona pomyślnie, prosimy spróbować ponownie.',
                    'error'
                );

                wp_redirect($orderObj->get_cancel_order_url());
                error_log(__METHOD__ . ' :(');
            }
        }


        function getBankHtml($bank_id, $bank_name, $text = '', $cc_id = '', $class = '', $onclick = '')
        {
            return '<a class="bank-box bank-item ' . $class . '" data-id="' . $bank_id . '" data-cc="' . $cc_id . '" onclick="' . $onclick . '">' .
            (empty($cc_id) ? '' : '<span class="removecc" ' .
                ' title="' . __('Usuń zapamiętaną kartę') . ' ' . $bank_name . ' ' . $text . '" ' .
                ' onclick="arguments[0].stopPropagation(); if (confirm(\'' . __('Czy na pewno?') . '\')) removecc(' . $cc_id . ')"></span>') .
            '<div class="bank-logo bank-logo-' . $bank_id . '">' .
            (empty($text) ? "" : "<span>{$text}</span>") .
            '</div><div class="bank-name">' . $bank_name . '</div></a>';
        }

        function getBankTxt(&$checkedCounter, $bank_id, $bank_name, $text = '', $cc_id = '', $class = '', $onclick = '')
        {
            $checkedCounter++;
            return
                '
                    <li onclick="' . $onclick . '">' .
                '<div class="input-box  bank-item" data-id="' . $bank_id . '" data-cc="' . $cc_id . '" data-text="' . $text . '" >' .
                '<label for="przelewy_method_id_' . $bank_id . '-' . $cc_id . '">' .
                '<input id="przelewy_method_id_' . $bank_id . '-' . $cc_id . '" name="payment_method_id" ' .
                ' class="radio" type="radio" ' . ($checkedCounter == 1 ? 'checked="checked"' : '') . ' />' .
                '<span>' . $bank_name . '</span></label>' .
                (empty($cc_id) ? '' : '<span class="removecc" ' .
                    ' title="' . __('Usuń zapamiętaną kartę') . ' ' . $bank_name . ' ' . $text . '" ' .
                    ' onclick="arguments[0].stopPropagation(); if (confirm(\'' . __('Czy na pewno?') . '\')) removecc(' . $cc_id . ')"></span>') .
                '</div></li>
                    ';
        }

        echo '<div style="clear:both"></div>';


        if (!empty($this->p24_api) && $conf['p24_paymethods'] == 'yes' && (int)get_post_meta($orderObj->id, 'p24_method', true) == 0) {

            $paymethod_all = $this->get_all_payment_methods($conf['p24_payslow'] == 'yes');

            // usunięcie rat gdy koszyk poniżej kwoty
            if (is_array($paymethod_all) && get_woocommerce_currency() == 'PLN' && $this->get_order_total() < Przelewy24Class::getMinRatyAmount()) {
                $raty = Przelewy24Class::getChannelsRaty();
                foreach ($paymethod_all as $key => $item) {
                    if (in_array($key, $raty)) {
                        unset($paymethod_all[$key]);
                    }
                }
            }

            $paymethod_first = explode(',', $conf['p24_paymethods_first']);
            $paymethod_second = explode(',', $conf['p24_paymethods_second']);
            $last_method = (int)$this->get_custom_data('user', get_current_user_id(), 'lastmethod');
            $accepted = (int)$this->get_custom_data('user', get_current_user_id(), 'accept');

            $ccards = $this->get_all_custom_data('user_cards', get_current_user_id());

            if ($conf['p24_accept'] == 'yes' && $accepted == 0) {
                echo
                    '<p><label for="p24_regulation_accept" style="font-weight: normal;">' .
                    '<input type="checkbox" name="p24_regulation_accept" id="p24_regulation_accept" style="display: inline-block;"> ' .
                    'Tak, zapoznałem się i akceptuję warunki <a target="_blank" href="http://www.przelewy24.pl/regulamin.htm">regulaminu</a> Przelewy24.pl.' .
                    '</label></p>';
            }

            $makeUnfold = false;

            echo <<<HTML
                    <script type="text/javascript">
                        jQuery.getScript('{$this->getJsUrl()}');
                    </script>
                    <input type="hidden" id="p24_css_url" value="{$this->getCssUrl()}">
                    <div class="payMethodList">
HTML;

            $ignore_array = array();
            if ($conf['p24_graphics'] == 'yes') {
                // lista graficzna

                // ostatnia metoda płatności
                if ($last_method > 0 && !in_array($last_method, Przelewy24Class::getChannelsCard())) {
                    if (isset($paymethod_all[$last_method])) {
                        $makeUnfold = true;
                        $ignore_array[] = $last_method;
                        echo getBankHtml($last_method, __('Ostatnio używane'));
                    }
                }

                // recuring
                if (is_array($ccards) && sizeof($ccards)) {
                    foreach ($ccards as $card) {
                        $makeUnfold = true;
                        echo getBankHtml(md5($card->custom_value['type']), $card->custom_value['type'], substr($card->custom_value['mask'], -9), $card->id, 'recurring');
                    }
                }

                // wyróżnione metody
                foreach ($paymethod_first as $bank_id) {
                    if (isset($paymethod_all[$bank_id]) && !in_array($bank_id, $ignore_array)) {
                        $makeUnfold = true;
                        $ignore_array[] = $bank_id;
                        $onclick = '';
                        if (in_array($bank_id, Przelewy24Class::getChannelsCard()) && $conf['p24_payinshop'] == 'yes') {
                            $onclick = 'showPayJsPopup()';
                        }
                        echo getBankHtml($bank_id, $paymethod_all[$bank_id], '', '', '', $onclick);
                    }
                }

                echo "<div style='clear:both'></div>";
                echo '<div class="morePayMethods" style="' . ($makeUnfold ? 'display: none' : '') . '">';
                // pozostałe metody płatności
                foreach ($paymethod_second as $bank_id) {
                    if (isset($paymethod_all[$bank_id]) && !in_array($bank_id, $ignore_array)) {
                        $ignore_array[] = $bank_id;
                        $onclick = '';
                        if (in_array($bank_id, Przelewy24Class::getChannelsCard()) && $conf['p24_payinshop'] == 'yes') {
                            $onclick = 'showPayJsPopup()';
                        }
                        echo getBankHtml($bank_id, $paymethod_all[$bank_id], '', '', '', $onclick);
                    }
                }

                // metody nieuwględnione w konfiguracji (np nowe)
                foreach ($paymethod_all as $bank_id => $bank_name) {
                    if (!in_array($bank_id, $paymethod_first) && !in_array($bank_id, $ignore_array)) {
                        $ignore_array[] = $bank_id;
                        $onclick = '';
                        if (in_array($bank_id, Przelewy24Class::getChannelsCard()) && $conf['p24_payinshop'] == 'yes') {
                            $onclick = 'showPayJsPopup()';
                        }
                        echo getBankHtml($bank_id, $paymethod_all[$bank_id], '', '', '', $onclick);
                    }
                }
                echo "<div style='clear:both'></div>";
                echo '</div>';
            } else {
                // lista tekstowa
                $checkedCounter = 0;

                // ostatnia metoda płatności
                if ($last_method > 0 && !in_array($last_method, Przelewy24Class::getChannelsCard())) {
                    if (isset($paymethod_all[$last_method])) {
                        $makeUnfold = true;
                        $ignore_array[] = $last_method;
                        echo getBankTxt($checkedCounter, $last_method, $paymethod_all[$last_method]);
                    }
                }

                // recuring
                if (is_array($ccards) && sizeof($ccards)) {
                    foreach ($ccards as $card) {
                        $makeUnfold = true;
                        echo getBankTxt($checkedCounter, md5($card->custom_value['type']), $card->custom_value['type'], substr($card->custom_value['mask'], -9), $card->id, 'recurring');
                    }
                }

                // wyróżnione metody
                foreach ($paymethod_first as $bank_id) {
                    if (isset($paymethod_all[$bank_id]) && !in_array($bank_id, $ignore_array)) {
                        $makeUnfold = true;
                        $ignore_array[] = $bank_id;
                        $onclick = '';
                        if (in_array($bank_id, Przelewy24Class::getChannelsCard()) && $conf['p24_payinshop'] == 'yes') {
                            $onclick = 'showPayJsPopup()';
                        }
                        echo getBankTxt($checkedCounter, $bank_id, $paymethod_all[$bank_id], '', '', '', $onclick);
                    }
                }

                echo "<div style='clear:both'></div>";
                echo '<div class="morePayMethods" style="' . ($makeUnfold ? 'display: none' : '') . '">';
                // pozostałe metody płatności
                foreach ($paymethod_second as $bank_id) {
                    if (isset($paymethod_all[$bank_id]) && !in_array($bank_id, $ignore_array)) {
                        $ignore_array[] = $bank_id;
                        $onclick = '';
                        if (in_array($bank_id, Przelewy24Class::getChannelsCard()) && $conf['p24_payinshop'] == 'yes') {
                            $onclick = 'showPayJsPopup()';
                        }
                        echo getBankTxt($checkedCounter, $bank_id, $paymethod_all[$bank_id], '', '', '', $onclick);
                    }
                }

                // metody nieuwględnione w konfiguracji (np nowe)
                foreach ($paymethod_all as $bank_id => $bank_name) {
                    if (!in_array($bank_id, $paymethod_first) && !in_array($bank_id, $ignore_array)) {
                        $ignore_array[] = $bank_id;
                        $onclick = '';
                        if (in_array($bank_id, Przelewy24Class::getChannelsCard()) && $conf['p24_payinshop'] == 'yes') {
                            $onclick = 'showPayJsPopup()';
                        }
                        echo getBankTxt($checkedCounter, $bank_id, $paymethod_all[$bank_id], '', '', '', $onclick);
                    }
                }

                echo "<div style='clear:both'></div>";
                echo '</div>';
            }

            if ($makeUnfold)
                echo '<div class="moreStuff" onclick="jQuery(this).fadeOut(100);jQuery(\'.morePayMethods\').slideDown()" title="Pokaż więcej metod płatności"></div>';
            echo "</div>";


            if ($conf['p24_payinshop'] == 'yes') {
                $p24_ajax_url = add_query_arg(array('wc-api' => 'WC_Gateway_Przelewy24'), home_url('/'));
                $translate = array(
                    'name' => __('Imię i nazwisko'),
                    'nr' => __('Numer karty'),
                    'cvv' => __('CVV'),
                    'dt' => __('Data ważności'),
                    'pay' => __('Zapłać'),
                    '3ds' => __('Kliknij tutaj aby kontynuować zakupy')
                );
                echo <<<HTML
                    <div id="P24FormAreaHolder" onclick="hidePayJsPopup();" style="display: none"><div onclick="arguments[0].stopPropagation();" id="P24FormArea" class="popup"></div></div>
                    <input type="hidden" id="p24_ajax_url" value="{$p24_ajax_url}">
                    <input type="hidden" id="p24_dictionary" value='{"cardHolderLabel":"{$translate[name]}", "cardNumberLabel":"{$translate[nr]}", "cvvLabel":"{$translate[cvv]}", "expDateLabel":"{$translate[dt]}", "payButtonCaption":"{$translate[pay]}", "threeDSAuthMessage":"{$translate['3ds']}"}'>
                    <input type="hidden" id="p24_woo_order_id" value='{$order}'>
                    <form method="post" id="cardrm">
                        <input type="hidden" name="act" value="cardrm">
                        <input type="hidden" name="cardrm">
                    </form>
HTML;

            }

            echo $this->generate_przelewy24_form($order, false, $conf['p24_oneclick'] == 'yes' && is_array($ccards) && sizeof($ccards));

        } else {

            echo $this->generate_przelewy24_form($order, true);
        }
    }

    public function getCssUrl()
    {
        return PRZELEWY24_URI . 'assets/css/paymethods.css';
    }

    public function getJsUrl()
    {
        return PRZELEWY24_URI . 'assets/js/payment.js';
    }

    /**
     * Generate przelewy24 button link
     **/
    private function generate_fields_array($order_id, $transaction_id = null)
    {
        global $woocommerce, $locale;

        $localization = !empty($locale) ? explode("_", $locale) : 'pl';
        $order = new WC_Order($order_id);
        if (!$order) return false;

        if (is_null($transaction_id)) {
            $transaction_id = $order_id . "_" . uniqid(md5($order_id . '_' . date("ymds")), true);
        }
        $redirect_url = get_site_url();

        // modifies order number if Sequential Order Numbers Pro plugin is installed
        if (class_exists('WC_Seq_Order_Number_Pro')) {
            $seq = new WC_Seq_Order_Number_Pro();
            $description_order_id = $seq->get_order_number($order_id, $order);
        } else if (class_exists('WC_Seq_Order_Number')) {
            $seq = new WC_Seq_Order_Number();
            $description_order_id = $seq->get_order_number($order_id, $order);
        } else {
            $description_order_id = $order_id;
        }

        //p24_opis depend of test mode
        $desc = ($this->p24_testmod == "sandbox" ? "Transakcja testowa, " : '') .
            'Zamówienie nr: ' . $description_order_id . ', ' . $order->billing_first_name . ' ' . $order->billing_last_name . ', ' . date('Ymdhi');

        //return address URL
        $payment_page = add_query_arg(array('wc-api' => 'WC_Gateway_Przelewy24', 'order_id' => $order_id), home_url('/'));
        $status_page = add_query_arg(array('wc-api' => 'WC_Gateway_Przelewy24'), home_url('/'));

        // jeśli jest basic-auth
        if (strlen($this->p24_basicauth)) {
            list($protocol, $url) = explode('://', $status_page, 2);
            $status_page = $protocol . '://' . $this->p24_basicauth . '@' . $url;
        }

        $accept = (int)$this->get_custom_data('user', get_current_user_id(), 'accept');

        /*Form send to przelewy24*/

        $amount = $order->order_total * 100;
        $amount = number_format($amount, 0, "", "");

        $p24_settings = get_option('woocommerce_przelewy24_settings');

        $currency = strtoupper($order->get_order_currency());
        $przelewy24_arg = array(
            'p24_session_id' => $transaction_id,
            'p24_merchant_id' => $this->merchant_id,
            'p24_pos_id' => $this->shop_id,
            'p24_email' => $order->billing_email,
            'p24_amount' => $amount,
            'p24_currency' => $currency,
            'p24_description' => $desc,
            'p24_language' => $localization[0],
            'p24_client' => $order->billing_first_name . ' ' . $order->billing_last_name,
            'p24_address' => $order->billing_address_1,
            'p24_city' => $order->billing_city,
            'p24_zip' => $order->billing_postcode,
            'p24_country' => $order->billing_country,
            'p24_encoding' => 'UTF-8',
            'p24_url_status' => $status_page,
            'p24_url_return' => $payment_page,
            'p24_url_cancel' => $payment_page,
            'p24_api_version' => P24_VERSION,
            'p24_ecommerce' => 'WooCommerce ' . WOOCOMMERCE_VERSION,
            'p24_method' => (int)get_post_meta($order->id, 'p24_method', true),
            'p24_regulation_accept' => $accept ? 'true' : '',
            'p24_time_limit' => !empty($this->p24_timelimit) ? $this->p24_timelimit : 15,
            'p24_channel' => $this->p24_payslow == 'yes' ? '16' : '',
            'p24_wait_for_result' => ($p24_settings['p24_wait_for_result'] == 'yes') ? 1 : 0,
            'p24_shipping' => number_format($order->get_total_shipping() * 100, 0, '', ''),
        );

        $lp = 0;
        foreach ($order->get_items() as $item) {
            $lp++;

            $przelewy24_arg['p24_name_' . $lp] = $item['name'];
            $przelewy24_arg['p24_description_' . $lp] = strip_tags(get_post($item['product_id'])->post_content);
            $przelewy24_arg['p24_quantity_' . $lp] = $item['qty'];
            $item_price = ($item['line_total'] / $item['qty']) * 100;
            $przelewy24_arg['p24_price_' . $lp] = number_format($item_price, 0, '', '');
            $przelewy24_arg['p24_number_' . $lp] = $item['product_id'];
        }
        $P24 = new Przelewy24Class($this->merchant_id, $this->shop_id, $this->salt, ($this->p24_testmod == 'sandbox'));
        $przelewy24_arg['p24_sign'] = $P24->trnDirectSign($przelewy24_arg);
        $P24->checkMandatoryFieldsForAction($przelewy24_arg, 'trnDirect');

        return $przelewy24_arg;
    }

    public function generate_przelewy24_form($order_id, $autoSubmit = true, $makeRecuringForm = false)
    {
        global $woocommerce;
        $order = new WC_Order($order_id);
        $przelewy24_arg = $this->generate_fields_array($order_id);
        $przelewy_form = '';
        foreach ($przelewy24_arg as $key => $value)
            $przelewy_form .= '<input type="hidden" name="' . $key . '" value="' . $value . '"/>';

        $P24 = new Przelewy24Class($this->merchant_id, $this->shop_id, $this->salt, ($this->p24_testmod == 'sandbox'));
        $return = '<div id="payment" style="background: none"> ' .
            '<form action="' . $P24->trnDirectUrl() . '" method="post" id="przelewy_payment_form"' .
            ($autoSubmit ? '' : ' onSubmit="return p24_processPayment()" ') .
            '>' .
            $przelewy_form .
            '<input type="submit" class="button alt" id="place_order" value="Zapłać" /> ' .
            '<a class="button cancel" href="' . $order->get_cancel_order_url() . '">Anuluj zamówienie</a>' .
            ($autoSubmit ?
                '<script type="text/javascript">jQuery(function(){jQuery("body").block({message: "' .
                __('Dziękujemy za złożenie zamówienia. Za chwilę nastąpi przekierowanie na stronę przelewy24.pl') .
                '",overlayCSS: {background: "#fff",opacity: 0.6},css: {padding:20,textAlign:"center",color:"#555",border:"2px solid #AF2325",backgroundColor:"#fff",cursor:"wait",lineHeight:"32px"}});' .
                'jQuery("#przelewy_payment_form input[type=submit]").click();});' .
                '</script>' : '') .
            '</form>' .
            '</div>' .
            '';
        if (!!$makeRecuringForm) {
            $return .= <<<FORMRECURING
                        <form method="post" id="przelewy24FormRecuring" name="przelewy24FormRecuring" accept-charset="utf-8">
                            <input type="hidden" name="p24_session_id" value="{$przelewy24_arg[p24_session_id]}" />
                            <input type="hidden" name="p24_cc" />
                        </form>
FORMRECURING;
        }
        return $return;
    }

    /**
     * Process the payment and return the result
     **/
    function process_payment($order_id)
    {
        global $woocommerce;
        $order = new WC_Order($order_id);
        return array('result' => 'success', 'redirect' => $order->get_checkout_payment_url($order));
    }

    /**
     * /*Check przelewy24 response
     **/
    function przelewy24_response()
    {
        global $woocommerce;
        $P24 = new Przelewy24Class($this->merchant_id, $this->shop_id, $this->salt, ($this->p24_testmod == 'sandbox'));

        $debug = false;
        if (($this->p24_debug == 'yes') and isset($_GET['test']) and $_GET['test'] == md5('p24' . $this->salt . 'debug')) $debug = true;
        if ($debug) {
            echo 'v. 3.2.9<br /><pre>';
            var_dump($P24->testConnection());
            echo "\n\nPOST:\n";
            var_dump($_POST);
            echo "\n\n";
            var_dump($this->merchant_id);
            var_dump($this->shop_id);
            echo "\n";
        }
        if (isset($_POST['p24_session_id']) && isset($_POST['action']) && $_POST['action'] == 'trnRegister' && isset($_POST['order_id'])) {
            $P24C = new Przelewy24Class($this->merchant_id, $this->shop_id, $this->salt, ($this->p24_testmod == 'sandbox'));
            $post_data = $this->generate_fields_array($_POST['order_id'], $_POST['p24_session_id']);
            foreach ($post_data as $k => $v) {
                $P24C->addValue($k, $v);
            }
            $token = $P24C->trnRegister();
            if (is_array($token)) {
                $token = $token['token'];
                exit(json_encode(array(
                    'p24jsURL' => $P24C->getHost() . 'inchtml/ajaxPayment/ajax.js?token=' . $token,
                    'p24cssURL' => $P24C->getHost() . 'inchtml/ajaxPayment/ajax.css',
                )));
            }

            exit();
        }

        if (isset($_POST['p24_session_id'])) {
            $p24_session_id = $_POST['p24_session_id'];
            $reg_session = "/^[0-9a-zA-Z_\.]+$/D";
            if (!preg_match($reg_session, $p24_session_id)) exit;
            $session_id = explode('_', $p24_session_id);
            $order_id = $session_id[0];
            $order = new WC_Order($order_id);

            $validation = array('p24_amount' => number_format($order->order_total * 100, 0, "", ""));
            $WYNIK = $P24->trnVerifyEx($validation);
            if ($WYNIK === null) {
                exit("\n" . 'MALFORMED POST');
            } elseif ($WYNIK === true) {
                $order->add_order_note(__('IPN payment completed', 'woocommerce'));
                $order->payment_complete();

                // zapis ostatniej metody płatności
                if ((int)$_POST['p24_method']) {
                    $this->set_custom_data('user', $order->get_user_id(), 'lastmethod', (int)$_POST['p24_method']);
                    $this->set_custom_data('user', $order->get_user_id(), 'accept', 1);

                    // jeśli karta i ma recuring to zapisz
                    if (in_array($_POST['p24_method'], Przelewy24Class::getChannelsCard()) && $this->p24_oneclick == 'yes') {
                        $this->saveCard((int)$order->get_user_id(), (int)$_POST['p24_order_id']);
                    }

                }
                if ($debug) exit("\n" . 'OK');
            } else {
                $order->update_status('failed');
                $order->cancel_order();
                if ($debug) {
                    var_dump($validation);
                    exit("\n" . 'ERROR');
                }
            }
            if (!isset($_GET['order_id'])) exit;
        }
        if ($debug) exit;
        if (isset($_GET['order_id']) && !$debug) {
            $order = new WC_Order($_GET['order_id']);

            if ($order->status == 'failed') {
                $this->addNotice(
                // Sorry your transaction did not go through successfully, please try again.
                    $woocommerce,
                    __('Payment error: ', 'woothemes') . 'Przepraszamy, ale twoja transakcja nie została przeprowadzona pomyślnie, prosimy spróbować ponownie.',
                    'error'
                );

                wp_redirect($order->get_cancel_order_url_raw());
            } else if ($order->status == 'completed' || $order->status == 'processing') {
                $woocommerce->cart->empty_cart();
                $order->update_status($this->p24_orderstate_after);
                //$this->orderDone($order);
                wp_redirect($this->get_return_url($order));

            } else {
                // We did not received information about payment. If you are sure you completed your payment please contact our customer service
                $this->addNotice(
                    $woocommerce,
                    'Płatność realizowana przez Przelewy24 nie została jeszcze potwierdzona. Jeśli potwierdzenie nadejdzie w czasie późniejszym, płatność zostanie automatycznie przekazana do sklepu',
                    'notice'
                );

                wp_redirect($this->get_return_url($order));
                //wp_redirect( $order->get_cancel_order_url_raw() );
            }
        }
    }

    function addNotice($woocommerce, $message, $type)
    {
        if ($type == 'error' && method_exists($woocommerce, 'add_error')) {
            $woocommerce->add_error($message);
        } else if (in_array($type, array('success', 'notice')) && method_exists($woocommerce, 'add_message')) {
            $woocommerce->add_message($message);
        } else {
            wc_add_notice($message, $type);
        }
    }

    function get_options()
    {
        $option_list = array();
        $option_list['secure'] = 'normalny';
        $option_list['sandbox'] = 'testowy';

        return $option_list;
    }

    function get_options_installment()
    {
        $option_list = array(
            0 => __('Ukryj wszystko'),
            1 => __('Przycisk (strona płatności)'),
            2 => __('Przycisk (strona płatności) i kalkulator rat (strona produktu)'),
        );

        return $option_list;
    }


    /**
     * Output for the order received page.
     */

    function thankyou_page()
    {
        if ($this->instructions) {
            echo wpautop(wptexturize($this->instructions));
        }
    }

    /**
     * Add content to the WC emails.
     *
     * @access public
     * @param WC_Order $order
     * @param bool $sent_to_admin
     * @param bool $plain_text
     */

    function email_instructions($order, $sent_to_admin, $plain_text = false)
    {
        if ($this->instructions && !$sent_to_admin && 'przelewy24' === $order->payment_method) {
            echo wpautop(wptexturize($this->instructions)) . PHP_EOL;
        }
    }

    private static function get_custom_data($data_type, $data_id, $key)
    {
        global $wpdb;
        $table_name = $wpdb->prefix . 'woocommerce_p24_custom_data';

        $fields = $wpdb->get_results(
            "SELECT * FROM {$table_name} WHERE data_type = '{$data_type}' AND data_id = {$data_id} AND custom_key = '{$key}'",
            OBJECT
        );
        foreach ($fields as $field) {
            $value = json_decode($field->custom_value, true);
            if ($value != null) return $value;
            else return $field->custom_value;
        }
        return null;
    }

    private static function get_all_custom_data($data_type, $data_id)
    {
        global $wpdb;
        $table_name = $wpdb->prefix . 'woocommerce_p24_custom_data';

        $fields = $wpdb->get_results(
            "SELECT * FROM {$table_name} WHERE data_type = '{$data_type}' AND data_id = {$data_id} ",
            OBJECT
        );
        foreach ($fields as &$field) {
            $value = json_decode($field->custom_value, true);
            if ($value != null) $field->custom_value = $value;
        }
        return $fields;
    }

    public static function get_all_cards($user_id)
    {
        $user_id = (int)$user_id;
        return self::get_all_custom_data('user_cards', $user_id);
    }

    public static function del_card($user_id, $card_id)
    {
        global $wpdb;
        $table_name = $wpdb->prefix . 'woocommerce_p24_custom_data';
        $card = self::getCard($user_id, $card_id);
        if ($card) {
            $key = md5($card->custom_value['mask'] . '|' . $card->custom_value['type'] . '|' . $card->custom_value['exp']);
            $wpdb->query("DELETE FROM {$table_name} WHERE data_type = 'user_cards' AND data_id = {$user_id} AND custom_key = '{$key}'");
            return true;
        }
        return false;

    }

    public static function get_cc_forget($user_id)
    {
        return (int)self::get_custom_data('user', $user_id, 'cc_forget');
    }

    public static function set_cc_forget($user_id, $value)
    {
        self::set_custom_data('user', $user_id, 'cc_forget', (int)$value == 1);
    }

    private static function set_custom_data($data_type, $data_id, $key, $value)
    {
        global $wpdb;
        $table_name = $wpdb->prefix . 'woocommerce_p24_custom_data';

        $wpdb->query("DELETE FROM {$table_name} WHERE data_type = '{$data_type}' AND data_id = {$data_id} AND custom_key = '{$key}'");

        if (empty($value)) return;

        if (is_object($value) || is_array($value)) $value = json_encode($value);

        $wpdb->insert($table_name, array(
            'data_type' => $data_type,
            'data_id' => $data_id,
            'custom_key' => $key,
            'custom_value' => $value,
        ), array('%s', '%d', '%s', '%s'));
    }

    private static function getCard($user_id, $card_id)
    {
        $all = self::get_all_custom_data('user_cards', $user_id);
        foreach ($all as $item) {
            if ($item->id == $card_id) return $item;
        }
        return false;
    }

    private function saveCard($user_id, $order_id)
    {
        if ($user_id > 0 && $this->p24_oneclick == 'yes' && strlen($this->p24_api) == 32) {

            if (self::get_cc_forget($user_id)) return; // nie zapamiętuj karty - Customer sobie nie życzy

            try {
                $P24 = new Przelewy24Class($this->merchant_id, $this->shop_id, $this->salt, ($this->p24_testmod == 'sandbox'), $this->p24_api);
                $s = new SoapClient($P24->getHost() . $P24->getWsdlCCService(), array('trace' => true, 'exceptions' => true));

                $res = $s->GetTransactionReference($this->merchant_id, $this->p24_api, $order_id);
                if ($res->error->errorCode === 0) {
                    $ref = $res->result->refId;
                    $exp = substr($res->result->cardExp, 2, 2) . substr($res->result->cardExp, 0, 2);
                    if (!empty($ref)) {
                        $hasRecurency = $s->CheckCard($this->merchant_id, $this->p24_api, $ref);
                        if ($hasRecurency->error->errorCode === 0 && $hasRecurency->result == true) {
                            if (date('ym') <= $exp) {
                                $this->set_custom_data('user_cards', $user_id, md5($res->result->mask . '|' . $res->result->cardType . '|' . $exp), array(
                                    'ref' => $ref,
                                    'exp' => $exp,
                                    'mask' => $res->result->mask,
                                    'type' => $res->result->cardType,
                                    'time' => date('Y-m-d H:i.s'),
                                ));
                            } else {
                                error_log(__METHOD__ . ' termin ważności ' . var_export($exp, true));
                            }
                        } else {
                            error_log(__METHOD__ . ' nie ma rekurencji ' . var_export($hasRecurency, true));
                        }
                    }
                }
            } catch (Exception $e) {
                error_log(__METHOD__ . ' ' . $e->getMessage());
            }
        }

    }

    private function chargeCard($order_id, $card_id)
    {
        $card = $this->getCard(get_current_user_id(), $card_id);
        $data = $this->generate_fields_array($order_id);

        if ($data && $card) {

            if (empty($card->custom_value['ref'])) return false;

            $P24 = new Przelewy24Class($this->merchant_id, $this->shop_id, $this->salt, ($this->p24_testmod == 'sandbox'), $this->p24_api);

            try {
                $s = new SoapClient($P24->getHost() . $P24->getWsdlCCService(), array('trace' => true, 'exceptions' => true));
                $res = $s->ChargeCard(
                    $data['p24_merchant_id'], $this->p24_api, $card->custom_value['ref'], $data['p24_amount'], $data['p24_currency'],
                    $data['p24_email'], $data['p24_session_id'], $data['p24_client'], $data['p24_description']
                );
                return $res->error->errorCode === 0;
            } catch (Exception $e) {
                if ($debug) echo $e->getMessage();
                error_log(__METHOD__ . ' ' . $e->getMessage());
            }
        }
        return false;
    }

    public static function getExtrachargeAmount($total)
    {
        $extracharge_amount = 0;

        $p24_settings = get_option('woocommerce_przelewy24_settings');
        $amount = round($total * 100, 0);

        if ($p24_settings['p24_extracharge'] == 'yes' && $p24_settings['p24_extracharge_product'] > 0 && $amount > 0) {
            $inc_amount_settings = (float)$p24_settings['p24_extracharge_amount'];
            $inc_percent_settings = (float)$p24_settings['p24_extracharge_percent'];

            $inc_amount = round($inc_amount_settings > 0 ? $inc_amount_settings * 100 : 0);
            $inc_percent = round($inc_percent_settings > 0 ? $inc_percent_settings / 100 * $amount : 0);

            $extracharge_amount = max($inc_amount, $inc_percent);

        }
        return $extracharge_amount;
    }

}