<div style="clear:both;" id="zenCards"></div>
<div style="clear:both;" id="zenDiscount"></div>
<div style="display: none;" id="cartTotalAmountZen"><?php print $data['cartProductsAmount']; ?></div>

<script>
    jQuery(document).ready(function () {

        var totalPriceContent = false;
        Zencard.run(function () {

            if (typeof listen === "undefined" || typeof listen === "object") {
                console.log('Running ZenCard and adding event listeners');
                Zencard.listen('discountEvent', function discountEventShopHandler(eventData) {
                    if (eventData.zCode !== null) {
                    }
                });
            }

            var customWriteAmountWithDiscount = function
                customWriteAmountWithDiscount(amountWithDiscountObj) {

                var amount = jQuery('.order-total .woocommerce-Price-amount.amount').html();

                if (amount == undefined) {
                    return false;
                }

                var match = amount.match(/[+\-]?\d+(,\d+)?(\.\d+)?/);

                if (match) {
                    match = match[0].replace(/\s/g, "");
                    var total = parseFloat(match.replace(',', ''));

                    var totalProductsAmount = jQuery('#cartTotalAmountZen').html().trim();

                    totalProductsAmount = parseFloat(totalProductsAmount);

                    var discount = totalProductsAmount - parseFloat(amountWithDiscountObj.major + '.' + amountWithDiscountObj.minor);
                    if (discount <= 0) {
                        return false;
                    }
                    var totalText = total - discount;
                    totalText = totalText.toFixed(2);

                    var newAmount = amount.replace(/[+\-]?\d+(,\d+)?(\.\d+)?/, totalText);
                    var label = "(z rabatem ZenCard)";
                    newAmount = newAmount.replace(label, '');
                    totalPriceContent = newAmount + ' <span class="zen-card-small-description">' + label + '</span>';

                    jQuery('.order-total .woocommerce-Price-amount.amount').html(totalPriceContent);

                    setTimeout(function () { // woocommerce post update
                        jQuery('.order-total .woocommerce-Price-amount.amount').html(totalPriceContent);
                        checkTotalPriceContentWasChanged();
                    }, 1000);
                }
            };

            Zencard.config({
                couponElementPath: '#zenCards',
                basketAmountPath: '#cartTotalAmountZen',
                amountWithDiscountPath: '#zenDiscount',
                writeAmountWithDiscount: customWriteAmountWithDiscount
            });
            Zencard.go();
        });

        function checkTotalPriceContentWasChanged() {
            setTimeout(function () {
                if (totalPriceContent && totalPriceContent != jQuery('.order-total .woocommerce-Price-amount.amount').html()) {
                    location.reload();
                } else {
                    checkTotalPriceContentWasChanged();
                }
            }, 500);
        }
    });
</script>

