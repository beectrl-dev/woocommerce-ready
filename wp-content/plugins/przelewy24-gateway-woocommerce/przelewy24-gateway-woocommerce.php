<?php
/**
 * Plugin Name: WooCommerce Przelewy24 Payment Gateway
 * Plugin URI: http://www.przelewy24.pl/pobierz
 * Description: Przelewy24 Payment gateway for woocommerce.
 * Version: 3.2.9
 * Author: DialCom24 Sp. z o.o.
 * Author URI: http://www.przelewy24.pl/
 */
session_start();

defined('ABSPATH') or die('No script kiddies please!');
define('PRZELEWY24_PATH', dirname(__FILE__));
define('PRZELEWY24_URI', plugin_dir_url(__FILE__));

register_activation_hook(__FILE__, 'woocommerce_gateway_przelewy24_activate');

function woocommerce_gateway_przelewy24_activate()
{
    global $wpdb;
    $wpdb->query('
      CREATE TABLE IF NOT EXISTS ' . $wpdb->prefix . 'woocommerce_p24_custom_data (
        id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
        data_type VARCHAR(32) NOT NULL,
        data_id INT NOT NULL,
        custom_key VARCHAR(32) NOT NULL,
        custom_value TEXT,
        INDEX search_key (data_type, data_id, custom_key),
        INDEX get_key (data_type, data_id)
      ) ENGINE = MYISAM;
    ');
}

register_uninstall_hook(__FILE__, 'woocommerce_gateway_przelewy24_uninstall');
function woocommerce_gateway_przelewy24_uninstall()
{
    delete_option('woocommerce_przelewy24_settings');

    global $wpdb;
    $table_name = $wpdb->prefix . 'woocommerce_p24_custom_data';

    $wpdb->query("DROP TABLE IF EXISTS $table_name");
    wp_cache_flush();
}

add_action('plugins_loaded', 'woocommerce_gateway_przelewy24_init', 0);
function woocommerce_gateway_przelewy24_init()
{
    if (!class_exists('WC_Payment_Gateway')) return;
    require_once 'class_przelewy24.php';
    require_once(PRZELEWY24_PATH . "/zencard/ZenCardApi.php");
    require_once(PRZELEWY24_PATH . "/inc/WC_Gateway_Przelewy24.class.php");
    require_once(PRZELEWY24_PATH . "/inc/Przelewy24PageTemplater.class.php");

    /**
     * add_action.
     */
    function woocommerce_przelewy24_gateway($methods)
    {
        $methods[] = 'WC_Gateway_Przelewy24';
        return $methods;
    }

    add_filter('woocommerce_payment_gateways', 'woocommerce_przelewy24_gateway');

    function my_stored_cards()
    {
        $t['my_cards'] = __('Moje zapisane karty');
        $t['are_you_sure'] = __('Czy jesteś pewien?');
        $t['delete_card'] = __('Usuń kartę');
        $t['cc_forget'] = __('Nie pamiętaj moich kart');
        $t['no_cards'] = __('Brak zapamiętanych kart płatniczych');
        $t['save'] = __('Zapisz');

        if (isset($_GET['cardrm']) && (int)$_GET['cardrm'] > 0) {
            WC_Gateway_Przelewy24::del_card(get_current_user_id(), (int)$_GET['cardrm']);
        }

        if (isset($_POST['act']) && $_POST['act'] == 'cc_forget') {
            WC_Gateway_Przelewy24::set_cc_forget(get_current_user_id(), $_POST['cc_forget'] == '1');
            if ($_POST['cc_forget'] == '1') {
                $all_cards = WC_Gateway_Przelewy24::get_all_cards(get_current_user_id());
                if (is_array($all_cards)) {
                    foreach ($all_cards as $item) {
                        WC_Gateway_Przelewy24::del_card(get_current_user_id(), $item->id);
                    }
                }
            }
        }

        $cc_forget_checked = WC_Gateway_Przelewy24::get_cc_forget(get_current_user_id()) ? ' checked="checked" ' : '';
        $all_cards = WC_Gateway_Przelewy24::get_all_cards(get_current_user_id());
        echo <<<HTML
            <h2>{$t[my_cards]}</h2>
                <div id="my-stored-cards">
                <p>
                    <form method="post">
                        <label for="cc_forget">
                            <input type="hidden" name="act" value="cc_forget">
                            <input type="checkbox" name="cc_forget" id="cc_forget" {$cc_forget_checked} value="1" onChange="jQuery('#cc_forget_save').fadeIn()">
                            <span> {$t[cc_forget]} </span>
                            <button class="button" id="cc_forget_save" style="display:none"> {$t[save]} </button>
                        </label>
                    </form>
                </p>
HTML;
        if (is_array($all_cards) && sizeof($all_cards) > 0) {
            foreach ($all_cards as $item) {
                $ccard = $item->custom_value;
                $ccard['exp'] = substr($ccard['exp'], 0, 2) . '/' . substr($ccard['exp'], 2);
                echo <<<HTML
                    <div class="ccbox">
                        <h5 class="page-heading">{$ccard['type']}</h5>
                        <p>{$ccard['mask']}</p>
                        <p>{$ccard['exp']}</p>
                        <a class="button" href="?cardrm={$item->id}"
                            onclick="return confirm('{$t[are_you_sure]}');"
                            title="{$t[delete_card]}">
                            {$t[delete_card]}
                        </a>
                    </div>                
HTML;
            }
        } else {
            echo "<h5>{$t[no_cards]}</h5>";
        }
        echo <<<HTML
            </div>
            <style>
                #my-stored-cards .ccbox {
                    background: #fbfbfb;
                    border: 1px solid #d6d4d4;
                    padding: 1em;
                    margin: 1em;
                    width: 40%;
                    display: inline-block;
                }
                #my-stored-cards .ccbox:nth-child(odd) { margin-left:1%; }
            </style>            
HTML;
    }

    add_action('woocommerce_after_my_account', 'my_stored_cards');

    function p24_woocommerce_single_product_summary()
    {
        global $product;
        $p24_settings = get_option('woocommerce_przelewy24_settings');
        if (get_woocommerce_currency() == 'PLN' && $product && $product->price >= Przelewy24Class::getMinRatyAmount() && $p24_settings['p24_installment'] == 2) {

            $jsonString = Przelewy24Class::requestGet('https://secure.przelewy24.pl/kalkulator_raty.php?ammount=' . ($product->price * 100) . '&format=json');
            $json = json_decode($jsonString);

            echo <<<HTML
                <a class="price-installment" target="_blank" href="https://secure.przelewy24.pl/kalkulator_raty/index.html?ammount={$product->price}">
                    {$json->ilosc_rat} rat x ~{$json->rata} zł
                </a>
HTML;
        }
    }

    add_action('woocommerce_single_product_summary', 'p24_woocommerce_single_product_summary');


    function p24_woocommerce_checkout_update_order_meta($order_id)
    {
        // zapis wybranej metody płatności na liście bramek
        if (empty($_POST['p24_method'])) $_POST['p24_method'] = 0;
        update_post_meta($order_id, 'p24_method', sanitize_text_field((int)$_POST['p24_method']));

        $extracharge_amount = WC_Gateway_Przelewy24::getExtrachargeAmount(wc_get_order($order_id)->get_total());

        if ($extracharge_amount > 0) {
            update_post_meta($order_id, 'p24_extracharge_amount', $extracharge_amount);
        }
    }

    add_action('woocommerce_checkout_update_order_meta', 'p24_woocommerce_checkout_update_order_meta');


    function p24_add_extracharge()
    {
        global $woocommerce, $wp;
        $order_id = (int)$wp->query_vars['order-pay'];
        $extracharge = ((int)get_post_meta($order_id, 'p24_extracharge_amount', true)) / 100;
        if ($extracharge > 0) {
            $p24_settings = get_option('woocommerce_przelewy24_settings');

            $product_id = $p24_settings['p24_extracharge_product'];

            update_post_meta($product_id, '_price', $extracharge);
            update_post_meta($product_id, '_sale_price', $extracharge);
            update_post_meta($product_id, '_regular_price', $extracharge);

            $product = wc_get_product($product_id);
            $order = wc_get_order($order_id);

            foreach ($order->get_items() as $itemId => $item) {
                if ($item['product_id'] == $product_id) {
                    wc_delete_order_item($itemId);
                }
            }
            $order->add_product($product, 1);
            $order->calculate_totals();
        }
    }

    add_action('before_woocommerce_pay', 'p24_add_extracharge');

    function p24_cart_totals()
    {
        renderZenCardHtml('cart');
    }

    add_action('woocommerce_after_cart_table', 'p24_cart_totals');


    function p24_checkout_form()
    {
        renderZenCardHtml('checkout');
    }

    add_action('woocommerce_after_checkout_form', 'p24_checkout_form');

    function p24_zencard_header()
    {
        $p24_settings = get_option('woocommerce_przelewy24_settings');
        $zenCardApi = new ZenCardApi($p24_settings['merchant_id'], $p24_settings['p24_api']);
        $zencardScript = $zenCardApi->getScript();
        if ($zencardScript) {
            echo $zencardScript;
        }
    }

    add_action('wp_head', 'p24_zencard_header');

    function ZenCardOrderProcessing($order_id)
    {
        $order = new WC_Order($order_id);
        $p24_settings = get_option('woocommerce_przelewy24_settings');
        if ($p24_settings['ZENCARD_PRZELEWY24_MODE'] == 'yes') {
            try {
                $zenCardApi = new ZenCardApi($p24_settings['merchant_id'], $p24_settings['p24_api']);
                $prefix = p24_zencard_coupon_prefix();
                if ($zenCardApi->isEnabled()) {
                    $zenCardCouponCode = null;
                    $orderCoupons = $order->get_items('coupon');
                    foreach ($orderCoupons as $coupon) {
                        if (substr($coupon['name'], 0, strlen($prefix)) == $prefix) {
                            $zenCardCouponCode = $coupon['name'];
                        }
                    }
                    $amount = number_format(($order->get_subtotal() + $order->cart_discount_tax + $order->get_cart_tax()) * 100, 0, "", "");
                    $zenCardOrderId = $zenCardApi->buildZenCardOrderId($zenCardCouponCode, $_SERVER['SERVER_NAME']);
                    $transactionConfirm = $zenCardApi->confirm($zenCardOrderId, $amount);
                    if ($transactionConfirm->isConfirmed()) {
                        $info = $transactionConfirm->getInfo();
                        if (isset($info)) {
                            $order->add_order_note($info);
                        }
                    }
                }
            } catch (Exception $e) {
                error_log(__METHOD__ . ' ' . $e->getMessage());
            }
        }
    }

    add_action('woocommerce_checkout_order_processed', 'ZenCardOrderProcessing');


    function ZenCardCheckoutProcess()
    {
        p24_add_zencard_discount();
    }

    add_action('woocommerce_checkout_process', 'ZenCardCheckoutProcess');


    /**
     * Others functions.
     */

    function renderZenCardHtml($page = '')
    {
        try {
            $p24_settings = get_option('woocommerce_przelewy24_settings');
            if ($p24_settings['ZENCARD_PRZELEWY24_MODE'] == 'yes' && $p24_settings['p24_api']) {
                global $woocommerce;
                $cartWithoutProducts = $woocommerce->cart->total - $woocommerce->cart->subtotal;
                $cartWithoutProducts = number_format($cartWithoutProducts, 2, ".", "");
                $cartProductsAmount = number_format($woocommerce->cart->subtotal, 2, ".", "");
                $params = array(
                    'cartProductsAmount' => $cartProductsAmount,
                    'page' => $page,
                );
                print Przelewy24PageTemplater::includeTemplate('zencard-box', $params);
            }
        } catch (Exception $e) {
            $error = new WP_Error();
            $error->add(601, $e->getMessage());
        }
    }


    function p24_add_zencard_discount()
    {
        global $woocommerce;
        $p24_settings = get_option('woocommerce_przelewy24_settings');
        if ($p24_settings['ZENCARD_PRZELEWY24_MODE'] == 'yes') {
            try {
                $zenCardApi = new ZenCardApi($p24_settings['merchant_id'], $p24_settings['p24_api']);

                $current_user = wp_get_current_user();

                if ($current_user->user_email) {
                    $identity = $current_user->user_email;
                } else {
                    $milliseconds = round(microtime(true) * 1000);
                    $identity = 'przelewy_' . $milliseconds . '@zencard.pl';
                }

                $amount = number_format($woocommerce->cart->subtotal * 100, 0, "", "");

                $prefix = p24_zencard_coupon_prefix();
                $coupon_code = uniqid($prefix);

                if ($zenCardApi->isEnabled()) {
                    $zenCardOrderId = $zenCardApi->buildZenCardOrderId($coupon_code, $_SERVER['SERVER_NAME']);
                    $transaction = $zenCardApi->verify($identity, $amount, $zenCardOrderId);

                    if ($transaction && $transaction->isVerified() && $transaction->hasDiscount()) {
                        $amount = $transaction->getAmountWithDiscount();
                        $without = $transaction->getAmount();
                        p24_zencard_set_coupon(round($without - $amount) / 100, $coupon_code);
                    }
                }
            } catch (Exception $e) {
                echo '<div class="woocommerce-info">Wystąpił błąd techniczny podczas użycia kuponu ZenCard. Kupon nie zostanie użyty.</div>';
                p24_zencard_del_coupons();
            }
        }
    }

    function p24_zencard_coupon_prefix()
    {
        return 'zencard';
    }


    function p24_zencard_del_coupons($exceptAmount = 0)
    {
        global $woocommerce;
        $coupons = $woocommerce->cart->get_coupons();
        $prefix = p24_zencard_coupon_prefix();
        $deleted = 0;

        // sprawdzenie już dodanych kuponów pod kątem usunięcia nieprawidłowych
        if (is_array($coupons)) {
            foreach ($coupons as $couponCode => $couponDetails) {
                // tylko kupony zencard
                if (substr($couponCode, 0, strlen($prefix)) == $prefix) {
                    // jeśli się nie zgadza kwota kuponów to je usuń
                    if (round($couponDetails->coupon_amount * 100, 0) != $exceptAmount || $exceptAmount == 0) {
                        $woocommerce->cart->remove_coupon($couponCode);
                        wp_delete_post($couponDetails->id);
                        $deleted++;
                    }
                }
            }
        }
        return $deleted;
    }

    function p24_zencard_set_coupon($amount = 0, $coupon_code)
    {
        global $woocommerce;
        $amount = (float)$amount;
        $prefix = p24_zencard_coupon_prefix();

        p24_zencard_del_coupons($amount);

        $coupons = $woocommerce->cart->get_coupons();
        $need_to_add_coupon = $amount > 0;
        // sprawdzenie już dodanych kuponów pod kątem występowania prawidłowego
        if ($need_to_add_coupon && is_array($coupons)) {
            foreach ($coupons as $couponCode => $couponDetails) {
                // tylko kupony zencard
                if (substr($couponCode, 0, strlen($prefix)) == $prefix) {
                    // jeśli jest już odpowiedni kupon to nie dodawaj nowego
                    if (round($couponDetails->coupon_amount * 100, 0) == $amount) {
                        $need_to_add_coupon = false;
                    }
                }
            }
        }

        if ($need_to_add_coupon) {
            $discount_type = 'fixed_cart';
            $coupon = array(
                'post_title' => $coupon_code,
                'post_content' => 'Kupon ZenCard',
                'post_excerpt' => 'Kupon ZenCard',
                'post_status' => 'publish',
                'post_author' => 1,
                'post_type' => 'shop_coupon'
            );

            $new_coupon_id = wp_insert_post($coupon);

            // Add meta
            update_post_meta($new_coupon_id, 'discount_type', $discount_type);
            update_post_meta($new_coupon_id, 'coupon_amount', $amount);
            update_post_meta($new_coupon_id, 'individual_use', 'yes');
            update_post_meta($new_coupon_id, 'product_ids', '');
            update_post_meta($new_coupon_id, 'exclude_product_ids', '');
            update_post_meta($new_coupon_id, 'usage_limit', '1');
            update_post_meta($new_coupon_id, 'expiry_date', '');
            update_post_meta($new_coupon_id, 'apply_before_tax', 'yes');
            update_post_meta($new_coupon_id, 'free_shipping', 'no');

            $woocommerce->cart->add_discount($coupon_code);
        }
    }
}
