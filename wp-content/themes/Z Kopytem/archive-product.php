<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see        https://docs.woocommerce.com/document/template-structure/
 * @author        WooThemes
 * @package    WooCommerce/Templates
 * @version     2.0.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}


get_header('shop'); ?>

<div class="reveal" id="products" data-reveal>
    <h1>Awesome. I Have It.</h1>
    <p class="lead">Your couch. It is mine.</p>
    <p>I'm a cool paragraph that lives inside of an even cooler modal. Wins!</p>
    <button class="close-button" data-close aria-label="Close modal" type="button">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="huge_button">
    <div class="row" data-equalizer>
        <div class="small-6 columns huge_button__side clearfix" data-equalizer-watch>
            <div class="huge_button__side__image"><img
                    src="<? echo get_template_directory_uri(); ?>/assets/images/logo_full.png"/></div>
        </div>
        <div class="small-6 columns huge_button__side" data-equalizer-watch>
            <a data-open="products" class=""><h2>Stwórz<br/>własna<br/><strong>czapkę</strong></h2></a></div>

    </div>
</div>


<div class="buy-your-hat">
    <?php
    /**
     * woocommerce_before_main_content hook.
     *
     * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
     * @hooked woocommerce_breadcrumb - 20
     */
    do_action('woocommerce_before_main_content');
    ?>

    <?php while (have_posts()) : the_post(); ?>
        <!--	--><? // do_action( 'woocommerce_before_single_product_summary' ); ?>

    <?php endwhile; // end of the loop. ?>

    <?php while (have_posts()) : the_post(); ?>

        <!--	--><?php //wc_get_template_part( 'content', 'single-product' ); ?>

    <?php endwhile; // end of the loop. ?>

    <?php
    /**
     * woocommerce_after_main_content hook.
     *
     * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
     */
    do_action('woocommerce_after_main_content');
    ?>

</div>
<?php get_footer('shop'); ?>
