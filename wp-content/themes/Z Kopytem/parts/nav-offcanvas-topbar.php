<!-- By default, this menu will use off-canvas for small
	 and a topbar for medium-up -->

<div class="top-bar nav__menu" id="top-bar-menu">
	<div class="top-bar-left float-left">
		<a href="<? echo home_url(); ?>"class="nav__menu__logo"><img src="<? echo get_template_directory_uri()?>/assets/images/logo.png"/></a>
		<?php joints_top_nav(); ?>
	</div>
	<div class="top-bar-right float-right show-for-small-only">
		<ul class="menu">
			<!-- <li><button class="menu-icon" type="button" data-toggle="off-canvas"></button></li> -->
			<li><a data-toggle="off-canvas"><?php _e( 'Menu', 'jointswp' ); ?></a></li>
		</ul>
	</div>
</div>