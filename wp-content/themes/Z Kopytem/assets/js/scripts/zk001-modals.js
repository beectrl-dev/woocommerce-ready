/**
 * Created by mkbeectrl on 26/10/2016.
 */

jQuery(document).ready(function($){
    /*var $obj=$(".zk-modal"),
        $overlay=$(".zk-modal--overlay"),
        blur=$("#blur-filter").get(0);

    function setBlur(v){
        blur.setAttribute("stdDeviation", v);
    }
    function getPos(){
        return $obj.position();
    }

    var lastPos=getPos();
    function update(){
        var pos=getPos();
        var limit=20;
        var dx=Math.min(limit,Math.abs(pos.left-lastPos.left)*0.5);
        var dy=Math.min(limit,Math.abs(pos.top-lastPos.top)*0.5);
        setBlur(dx+","+dy);

        lastPos=pos;
        requestAnimationFrame(update);
        console.log("dx = " + dx, "dy = " + dy);
    }
    update();

    var isOpen=false;
    function openModal(){

        TweenMax.to($overlay,0.1,{autoAlpha:1});

        TweenMax.fromTo($obj,0.6,{y:-($(window).height()+$obj.height())},{delay:0.2,y:"-50%",ease:Elastic.easeOut,easeParams:[1.1,0.7],force3D:true});
    }
    function closeModal(){
        TweenMax.to($overlay,0.1,{delay:0.55,autoAlpha:0});
        TweenMax.to($obj,0.55,{y:$(window).height()+$obj.height(),ease:Back.easeIn,force3D:true});
    }
    $(".zk-modal__open").click(function(){
        openModal();
        console.log("Opening modal");
    });
    $(".zk-modal__close, .zk-modal--overlay").click(function(){
        closeModal();
        console.log("Closing modal");
    })*/

    var revealStatus = false;
    $(window).on(
        'closeme.zf.reveal', function () {
            revealStatus = false;
            setStep(1);
        }
    );

    $(window).on(
        'closed.zf.reveal', function () {
            revealStatus = false;
            setStep(1);
        }
    );

    $(window).on(
        'open.zf.reveal', function () {
            revealStatus = true;
            setStep(1);
        }
    );
    !function () {
        $('.pompon-type').each(function () {
            var attribute = $(this).data('select-id');
            $('#' + attribute + ' option').each(function () {
                $(this).attr("selected", false);
            });
            $('#' + attribute + ' option[value=brak]').attr("selected", true);

        })
    }();

});
