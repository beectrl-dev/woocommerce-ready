jQuery(document).ready(function ($) {



        $('.pompon-type').on('click',
            function () {
                var attribute = $(this).data('select-id');
                var value = $(this).data('value')
                $('#' + attribute).val(value);

                $('#' + attribute + ' option').each(function () {
                    $(this).attr("selected", false);
                });
                $('#' + attribute + ' option[value=' + value + ']').attr("selected", true);
            })
        !function () {
            var image = '.buy-your-hat__step--one__container--left__image';
            $(image).on('click', function () {
                setProductVisible($(this).attr('id'));
            })
        }();
        function setProductVisible(id) {
            var product = '#' + id.replace("image-", "");
            $('.product').hide();
            // alert(product);
            $(product).css("display", "block");
            setStep(2)
        }

        var variation_one = '.variations > .variation:nth-child(1)';
        var variation_two = '.variations > .variation:nth-child(2)';
        var variation_three = '.variations > .variation:nth-child(3)';
        $(variation_one + ' img').on('click', function () {
                if (revealStatus)
                    setStep(3)
            }
        );
        function setStep(number) {
            function turnOffStep(strNumber) {
                $('.buy-your-hat__step--' + strNumber).hide();
            }

            function turnOnStep(strNumber) {
                $('.buy-your-hat__step--' + strNumber).show();
            }

            switch (number) {
                case 1:
                    $('#products').addClass('step-1');
                    $('#products').removeClass('step-2');
                    $('#products').removeClass('step-3');
                    break;
                case 2:

                    $('#products').removeClass('step-1');
                    $('#products').addClass('step-2');
                    $('#products').removeClass('step-3');
                    break;
                case 3:
                    $('#products').removeClass('step-1');
                    $('#products').removeClass('step-2');
                    $('#products').addClass('step-3');
                    break;
                default:
                    break;

                
            }
        }
    }
);