/*
jQuery(document).ready(function ($) {
        var revealStatus = false;

        function setStep(number) {
            function turnOffStep(strNumber) {
                $('.buy-your-hat__step--' + strNumber).hide();
            }

            function turnOnStep(strNumber) {
                $('.buy-your-hat__step--' + strNumber).show();
            }

            switch (number) {
                case 1:
                    $('#products').addClass('step-1');
                    $('#products').removeClass('step-2');
                    $('#products').removeClass('step-3');
                    break;
                case 2:

                    $('#products').removeClass('step-1');
                    $('#products').addClass('step-2');
                    $('#products').removeClass('step-3');
                    break;
                case 3:
                    $('#products').removeClass('step-1');
                    $('#products').removeClass('step-2');
                    $('#products').addClass('step-3');
                    break;
                default:
                    break;
            }
        }

        $(window).on(
            'closeme.zf.reveal', function () {
                revealStatus = false;
                setStep(1);
            }
        );

        $(window).on(
            'closed.zf.reveal', function () {
                revealStatus = false;
                setStep(1);
            }
        );

        $(window).on(
            'open.zf.reveal', function () {
                revealStatus = true;
                setStep(1);
            }
        );
        !function () {
            $('.pompon-type').each(function () {
                var attribute = $(this).data('select-id');
                $('#' + attribute + ' option').each(function () {
                    $(this).attr("selected", false);
                });
                $('#' + attribute + ' option[value=brak]').attr("selected", true);

            })
        }();


        $('.pompon-type').on('click',
            function () {
                var attribute = $(this).data('select-id');
                var value = $(this).data('value')
                $('#' + attribute).val(value);

                $('#' + attribute + ' option').each(function () {
                    $(this).attr("selected", false);
                });
                $('#' + attribute + ' option[value=' + value + ']').attr("selected", true);
            })
        !function () {
            var image = '.buy-your-hat__step--one__container--left__image';
            $(image).on('click', function () {
                setProductVisible($(this).attr('id'));
            })
        }();
        function setProductVisible(id) {
            var product = '#' + id.replace("image-", "");
            $('.product').hide();
            // alert(product);
            $(product).css("display", "block");
            setStep(2)
        }

        var variation_one = '.variations > .variation';
        var variation_two = '.variations > .variation:nth-child(2)';
        var variation_three = '.variations > .variation:nth-child(3)';
        $(variation_one + ' img').on('click', function () {
                if (revealStatus)
                    setStep(3)
            }
        );
    }
);
*/

jQuery(document).foundation();
/*
These functions make sure WordPress
and Foundation play nice together.
*/

jQuery(document).ready(function() {

    // Remove empty P tags created by WP inside of Accordion and Orbit
    jQuery('.accordion p:empty, .orbit p:empty').remove();

	 // Makes sure last grid item floats left
	jQuery('.archive-grid .columns').last().addClass( 'end' );

	// Adds Flex Video to YouTube and Vimeo Embeds
  jQuery('iframe[src*="youtube.com"], iframe[src*="vimeo.com"]').each(function() {
    if ( jQuery(this).innerWidth() / jQuery(this).innerHeight() > 1.5 ) {
      jQuery(this).wrap("<div class='widescreen flex-video'/>");
    } else {
      jQuery(this).wrap("<div class='flex-video'/>");
    }
  });

});

/*
jQuery(document).ready(function ($) {
        var revealStatus = false;
        $(window).on(
            'closeme.zf.reveal', function () {
                revealStatus = false;
                setStep(1);
            }
        );

        $(window).on(
            'closed.zf.reveal', function () {
                revealStatus = false;
                setStep(1);
            }
        );

        $(window).on(
            'open.zf.reveal', function () {
                revealStatus = true;
                setStep(1);
            }
        );
        !function () {
            $('.pompon-type').each(function () {
                var attribute = $(this).data('select-id');
                $('#' + attribute + ' option').each(function () {
                    $(this).attr("selected", false);
                });
                $('#' + attribute + ' option[value=brak]').attr("selected", true);

            })
        }();


        $('.pompon-type').on('click',
            function () {
                var attribute = $(this).data('select-id');
                var value = $(this).data('value')
                $('#' + attribute).val(value);

                $('#' + attribute + ' option').each(function () {
                    $(this).attr("selected", false);
                });
                $('#' + attribute + ' option[value=' + value + ']').attr("selected", true);
            })
        !function () {
            var image = '.buy-your-hat__step--one__container--left__image';
            $(image).on('click', function () {
                setProductVisible($(this).attr('id'));
            })
        }();
        function setProductVisible(id) {
            var product = '#' + id.replace("image-", "");
            $('.product').hide();
            // alert(product);
            $(product).css("display", "block");
            setStep(2)
        }

        var variation_one = '.variations > .variation   ';
        var variation_two = '.variations > .variation:nth-child(2)';
        var variation_three = '.variations > .variation:nth-child(3)';
        $(variation_one + ' img').on('click', function () {
            alert('test');
                if (revealStatus)
                    setStep(3)
            }
        );
        function setStep(number) {
            function turnOffStep(strNumber) {
                $('.buy-your-hat__step--' + strNumber).hide();
            }

            function turnOnStep(strNumber) {
                $('.buy-your-hat__step--' + strNumber).show();
            }

            switch (number) {
                case 1:
                    $('#products').addClass('step-1');
                    $('#products').removeClass('step-2');
                    $('#products').removeClass('step-3');
                    break;
                case 2:

                    $('#products').removeClass('step-1');
                    $('#products').addClass('step-2');
                    $('#products').removeClass('step-3');
                    break;
                case 3:
                    $('#products').removeClass('step-1');
                    $('#products').removeClass('step-2');
                    $('#products').addClass('step-3');
                    break;
                default:
                    break;
            }
        }
    }
);*/

/**
 * Created by mkbeectrl on 26/10/2016.
 */

jQuery(document).ready(function($){
    /*var $obj=$(".zk-modal"),
        $overlay=$(".zk-modal--overlay"),
        blur=$("#blur-filter").get(0);

    function setBlur(v){
        blur.setAttribute("stdDeviation", v);
    }
    function getPos(){
        return $obj.position();
    }

    var lastPos=getPos();
    function update(){
        var pos=getPos();
        var limit=20;
        var dx=Math.min(limit,Math.abs(pos.left-lastPos.left)*0.5);
        var dy=Math.min(limit,Math.abs(pos.top-lastPos.top)*0.5);
        setBlur(dx+","+dy);

        lastPos=pos;
        requestAnimationFrame(update);
        console.log("dx = " + dx, "dy = " + dy);
    }
    update();

    var isOpen=false;
    function openModal(){

        TweenMax.to($overlay,0.1,{autoAlpha:1});

        TweenMax.fromTo($obj,0.6,{y:-($(window).height()+$obj.height())},{delay:0.2,y:"-50%",ease:Elastic.easeOut,easeParams:[1.1,0.7],force3D:true});
    }
    function closeModal(){
        TweenMax.to($overlay,0.1,{delay:0.55,autoAlpha:0});
        TweenMax.to($obj,0.55,{y:$(window).height()+$obj.height(),ease:Back.easeIn,force3D:true});
    }
    $(".zk-modal__open").click(function(){
        openModal();
        console.log("Opening modal");
    });
    $(".zk-modal__close, .zk-modal--overlay").click(function(){
        closeModal();
        console.log("Closing modal");
    })*/

    var revealStatus = false;
    $(window).on(
        'closeme.zf.reveal', function () {
            revealStatus = false;
            setStep(1);
        }
    );

    $(window).on(
        'closed.zf.reveal', function () {
            revealStatus = false;
            setStep(1);
        }
    );

    $(window).on(
        'open.zf.reveal', function () {
            revealStatus = true;
            setStep(1);
        }
    );
    !function () {
        $('.pompon-type').each(function () {
            var attribute = $(this).data('select-id');
            $('#' + attribute + ' option').each(function () {
                $(this).attr("selected", false);
            });
            $('#' + attribute + ' option[value=brak]').attr("selected", true);

        })
    }();

});

jQuery(document).ready(function ($) {



        $('.pompon-type').on('click',
            function () {
                var attribute = $(this).data('select-id');
                var value = $(this).data('value')
                $('#' + attribute).val(value);

                $('#' + attribute + ' option').each(function () {
                    $(this).attr("selected", false);
                });
                $('#' + attribute + ' option[value=' + value + ']').attr("selected", true);
            })
        !function () {
            var image = '.buy-your-hat__step--one__container--left__image';
            $(image).on('click', function () {
                setProductVisible($(this).attr('id'));
            })
        }();
        function setProductVisible(id) {
            var product = '#' + id.replace("image-", "");
            $('.product').hide();
            // alert(product);
            $(product).css("display", "block");
            setStep(2)
        }

        var variation_one = '.variations > .variation:nth-child(1)';
        var variation_two = '.variations > .variation:nth-child(2)';
        var variation_three = '.variations > .variation:nth-child(3)';
        $(variation_one + ' img').on('click', function () {
                if (revealStatus)
                    setStep(3)
            }
        );
        function setStep(number) {
            function turnOffStep(strNumber) {
                $('.buy-your-hat__step--' + strNumber).hide();
            }

            function turnOnStep(strNumber) {
                $('.buy-your-hat__step--' + strNumber).show();
            }

            switch (number) {
                case 1:
                    $('#products').addClass('step-1');
                    $('#products').removeClass('step-2');
                    $('#products').removeClass('step-3');
                    break;
                case 2:

                    $('#products').removeClass('step-1');
                    $('#products').addClass('step-2');
                    $('#products').removeClass('step-3');
                    break;
                case 3:
                    $('#products').removeClass('step-1');
                    $('#products').removeClass('step-2');
                    $('#products').addClass('step-3');
                    break;
                default:
                    break;

                
            }
        }
    }
);