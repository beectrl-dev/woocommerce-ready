<?php
function site_scripts() {
  global $wp_styles; // Call global $wp_styles variable to add conditional wrapper around ie stylesheet the WordPress way

    // Load What-Input files in footer
    wp_enqueue_script( 'what-input', get_template_directory_uri() . '/vendor/what-input/what-input.min.js', array(), '', true );
    
    // Adding Foundation scripts file in the footer
    wp_enqueue_script( 'foundation-js', get_template_directory_uri() . '/assets/js/foundation.js', array( 'jquery' ), '6.2.3', true );
    
    // Adding scripts file in the footer
    wp_enqueue_script( 'site-js', get_template_directory_uri() . '/assets/js/scripts.js', array( 'jquery' ), '', true );
   
    // Register main stylesheet
    wp_enqueue_style( 'site-css', get_template_directory_uri() . '/assets/css/style.min.css', array(), '', 'all' );

    wp_enqueue_style( 'site-css-dev', get_template_directory_uri() . '/assets/css/style_dev.min.css', array(), '', 'all' );

    // Comment reply script for threaded comments
    if ( is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
      wp_enqueue_script( 'comment-reply' );
    }
}
add_action('wp_enqueue_scripts', 'site_scripts', 999);

$css = file_exists( get_stylesheet_directory() . '/woocommerce/yith_wccl.css' ) ? get_stylesheet_directory_uri() . '/woocommerce/yith_magnifier.css' : YITH_WCCL_URL . 'assets/css/frontend.css';

wp_register_script( 'yith_wccl_frontend', YITH_WCCL_URL . 'assets/js/frontend'. $suffix .'.js', array('jquery', 'wc-add-to-cart-variation'), '', true );
wp_register_style( 'yith_wccl_frontend', $css, false, '' );

if(1) {
    wp_enqueue_script( 'yith_wccl_frontend' );
    wp_enqueue_style( 'yith_wccl_frontend' );

    wp_localize_script( 'yith_wccl_frontend', 'yith_wccl_arg', array(
        'is_wc24' => version_compare( preg_replace( '/-beta-([0-9]+)/', '', $woocommerce->version ), '2.3', '>' )
    ));
}