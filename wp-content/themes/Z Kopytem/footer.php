					<footer class="footer" role="contentinfo">
						<div id="inner-footer" class="footer__inner">
							<div class="footer__inner__links">
								<nav role="navigation">
		    						<?php joints_footer_links(); ?>
		    					</nav>
		    				</div>
							<div class="footer__inner__copyright">
								<p class="source-org copyright">copyright <?php echo date('Y'); ?> | <?php bloginfo('name'); ?>.</p>
							</div>
						</div> <!-- end #inner-footer -->
					</footer> <!-- end .footer -->
				</div>  <!-- end .main-content -->
			</div> <!-- end .off-canvas-wrapper-inner -->
		</div> <!-- end .off-canvas-wrapper -->
		<?php wp_footer(); ?>
	</body>
</html> <!-- end page -->